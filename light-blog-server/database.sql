CREATE DATABASE blogdb;

CREATE TABLE bloglist(
    blog_id SERIAL PRIMARY KEY,
    blog_title VARCHAR(100),
    blog_body VARCHAR(200),
    blog_auther VARCHAR(50)
);