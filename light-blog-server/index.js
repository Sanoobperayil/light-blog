const express = require("express");
const app = express();
const cors = require("cors");
 const pool = require("./db");
app.listen(5000,()=>{
    console.log("server is stared on port 5000")
})
 
// middleware
app.use(cors());
app.use(express.json());

 // ROUTS//

 //create a blog with post
 app.post("/blog",async(req,res)=>{
     try {
     console.log(req.body)

    const {blog_title,blog_body,blog_auther}=req.body;
    const newblog = await pool.query(
        "INSERT INTO bloglist (blog_title,blog_body,blog_auther) VALUES($1,$2,$3) RETURNING *",
        [blog_title,blog_body,blog_auther]
    ); 
    res.json(newblog.rows[0,0,0]);
         
     } catch (error) {
         console.error(error.message); 
         
     }

 })

//post body


// app.post("/blog",async(req,res)=>{
//     try {
//     console.log(req.body)

//    const {blog_body}=req.body;
//    const newblog = await pool.query(
//        "INSERT INTO bloglist (blog_body) VALUES($1) RETURNING *",
//        [blog_body]
//    ); 
//    res.json(newblog.rows[0]);
        
//     } catch (error) {
//         console.error(error.message); 
        
//     }

// })



 //get all blog
 app.get("/blog",async(req,res)=>{
     try {
         const allblogs=await pool.query("SELECT * FROM bloglist");
         res.json(allblogs.rows);
     } catch (error) {
         console.error(error.message);
         
     }
 })


 //get a blog
 app.get("/blog/:id",async(req,res)=>{ 

    try {
       // console.log(req.params); 
         const { id } = req.params;
      const blog=await pool.query(" SELECT * FROM bloglist WHERE   blog_id=$1",[id]);
      res.json(blog.rows[0]);
 
    } catch (error) {
        console.error(error.message);
    }
})

 // udate a blog

 app.put("/blog/:id",async(req,res)=>{
     try {
         const {id}=req.params;
         const {blog_title,blog_body,blog_auther}=req.body; 
         const Updateblog =await pool.query("UPDATE bloglist SET blog_title=$1, blog_body=$2, blog_auther=$3 WHERE blog_id=$4 RETURNING *",[blog_title,blog_body,blog_auther,id] );
         res.json("blog updated");
         
     } catch (error) {
         
     }
 })




 //delete blog

 app.delete("/blog/:id",async(req,res)=>{
     try {
         const {id}=req.params;
         const deleteBlog = await pool.query("DELETE FROM bloglist WHERE blog_id=$1",[id]);
         res.json("a blog was deleted");
         
     } catch (error) {
         console.log(error.message);
         
     }
 })