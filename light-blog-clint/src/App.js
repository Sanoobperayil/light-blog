import React from 'react';

import './App.css';
import BlogInput from './component/BlogInput';
import ListBlog from './component/ListBlog';
function App() {
  return (
   <div>
     <BlogInput />
     <ListBlog />
    </div>
  );
}

export default App;
