import React, { Component } from "react";
import axios from "axios";


export default class BlogInput extends Component {
  state = {
    // articlename: "",
    // articlebody: "",
    // articleauther: "",
    blog_title: "",
    blog_body: "",
    blog_auther: "",
    // nameError: "",
    // bodyError: "",
    // autherError: "",
  };

  validation = () => {
    let nameError = "";
    let bodyError = "";
    let autherError = "";

    if (!this.state.blog_title) {
      nameError = "name canot empty";
    }
    if (!this.state.blog_body) {
      bodyError = "body canot empty";
    }
    if (!this.state.blog_auther) {
      autherError = "auther field canot empty";
    }

    if (nameError || bodyError || autherError) {
      this.setState({ nameError, bodyError, autherError });
      return false;
    }
    return true;
  };

  textChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  submit = async (e) => {
    e.preventDefault();
    const isValid = this.validation();
    if (isValid) {
      let data = {
        blog_title: this.state.blog_title,
        blog_body: this.state.blog_body,
        blog_auther: this.state.blog_auther,
      };
      console.log(data);

      // To fetch post data to the database =>> axios 

      axios
        .post("http://localhost:5000/blog", data)
        .then(function (response) {
          console.log(response);
        })
        
        .catch(function (error) {
          console.log(error);
        });

      //use
      // const body=this.blog_title;
      // const response = await fetch("http://localhost:5000/blog",{
      //   method :"POST",
      //   headers :{"Content-Type":"application/json"},
      //   body: JSON.stringify(this.blog_title)
      // });
      // console.log(this.blog_title);
      // console.log(response);
    }
  };

  render() {
    
    return (
      <div className="p-3 mb-2 bg-light text-dark">
        <div className="col-md-4 offset-md-4">
          <form onSubmit={this.submit} id="create-course-form">
            <h2 className="text-center">LightBlog</h2>
            <div className="form-group">
              <label></label>
              <input
                onChange={this.textChange}
                value={this.blog_title}
                type="text"
                className="form-control"
                name="blog_title"
                placeholder="Article Title"
              />
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.nameError}
              </div>
            </div>
            <div className="form-group">
              <label for="exampleFormControlTextarea1"></label>
              <textarea
                onChange={this.textChange}
                className="form-control"
                placeholder="Article Body"
                name="blog_body"
                value={this.blog_body}
                rows="3"
              ></textarea>
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.bodyError}
              </div>
            </div>
            <div className="form-group">
              <label></label>
              <input
                onChange={this.textChange}
                type="text area"
                className="form-control"
                name="blog_auther"
                placeholder="Article Auther"
                value={this.blog_auther}
              />
              <div style={{ fontSize: 12, color: "red" }}>
                {this.state.autherError}
              </div>
            </div>

            <button
              // onClick={this.submit}
              type="submit"
              className="btn btn-primary btn-lg float-right btn-sm"
            >
              Submit
            </button>
          </form>
          
        </div>
        
      </div>
    );
  }
}
