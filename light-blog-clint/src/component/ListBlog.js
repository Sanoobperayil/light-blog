import "./ListBlog.css";
import React, { Fragment, useEffect, useState } from "react";
const axios = require("axios");



export default function ListBlog() {
  const [blogs, setBlog] = useState([]);
  const deleteList = async (id) => {
    //  if(window.confirm("are you sure want to delete??"))
    /// alert(id)

    try {
      axios
        .delete("http://localhost:5000/blog/" + id, {
          method: "DELETE",
        })
        .then(function (response) {
          console.log("suss");
          setBlog(blogs.filter(blog =>blog.blog_id !==id))
        });

    } catch (error) {}
  };

  const getBlog = async () => {
    axios
      .get("http://localhost:5000/blog")
      .then(function (response) {
        // handle success
        setBlog(response.data);
        //  console.log(response);
        console.log("sucseds");
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      });
  };
  useEffect(() => {
    getBlog();
  }, []);
  console.log(blogs);
  return (
    <Fragment>
      {""}
      {
        <div className="p-3 mb-2 bg-light text-dark" >
          <ul className= "list-group list-group-flush">
            {blogs.map((blog) => (
              <div key={blog.blog_id} className="rounded">
                <li  className="list-group-item">{blog.blog_id}</li>
                <li className="list-group-item">{blog.blog_title}</li>
                <li className="list-group-item">{blog.blog_body}</li>
                <li  className="list-group-item">{blog.blog_auther}</li>
               
                <button type="button" class="btn btn-info btn-sm">
                  edit
                </button>
                <button
                  type="button"
                  className="btn btn-danger btn-sm"
                  
                  onClick={() => deleteList(blog.blog_id)}
                >
                  delete
                </button>
              </div>
            ))}
          </ul>
        </div>
      }
    </Fragment>
  );
}
